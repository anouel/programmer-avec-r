# Table des matières révisée de l'édition 2018

> Guider le lecteur à l'intérieur d'un chapitre au lieu de présenter
> les blocs de code d'un coup

## Programmer

+ expliquer ce qu'est la programmation et pourquoi c'est important
  pour à peu près tous les scientifiques
+ introduire le concept d'algorithme
+ regrouper Systèmes d'exploitation et systèmes de fichiers;
  introduction: «À titre de (futurs) programmeurs, vous devrez tirer
  profit au maximum des ressources de votre ordinateur. Ceci exige
  d'en connaitre le fonctionnement mieux que l'utilisateur lambda. Or,
  vos principaux outils numériques sont aujourd'hui les téléphones et
  tablettes aux interfaces hautement simplifiées.\par Connaissez-vous le
  rôle du système d'exploitation d'un ordinateur? En connaissez-vous
  les diverses variantes et leurs caractéristiques principales?
  Comprenez-vous bien le système de classification des fichiers de
  votre ordinateur? Vous devrez pouvoir répondre «oui» à ces
  questions. Cette section est là pour vous y aider.»
+ systèmes de fichiers: ajouter un mot sur les interfaces «Explorateur
  Windows» (dont le nom n'est plus affiché) et Finder; captures d'écran 
+ références: Swinnen:python:2012, Downey:python:2015

- ligne de commande

## Présentation de R

+ place prépondérante dans l'analyse de données
+ popularité grandissante du langage
+ utilisation des fichiers de script en fin de chapitre (?)
+ commentaires

## Premiers pas avec R

+ fonctions utiles: suites et répétition; head/tail; sommaires;
  arrondi; sommaires cumulatifs
+ opérateurs: ce qui se trouve à la section 7.1 dans version
  précédente
+ HOWTO: décomposition d'une expression complexe

- tout ce qui a trait à la composition de fonctions, SAUF l'appel de
  fonctions
  
## Fonctions (deux semaines)

+ matériel du chapitre 3
+ structures de contrôle (sections 6.1-6.5 inclusivement)
+ implémentation
+ style de codage (indentation, style, présentation, commentaires,
  porter attention à comment c'est présenté dans le document)
  
## Algorithmique

Penser à découper tout ça en deux
+ fonctions R de tri
+ fonctions R de recherche

## Structures de données

+ fonctions sur les matrices

## Fonctions d'application

+ section 6.6

## Paquetages

+ section 7.3

## Analyse et contrôle de texte

+ ligne de commande
+ redirection avant de parler de regex

## Travail collaboratif avec Git

## Débogage

## Programmation orientée objet
